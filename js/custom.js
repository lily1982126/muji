;(function($){
/*extend functions*/
$.fn.extend({
	_toolsbar:function(){  /* 工具栏专用      */
		var tog = $(this),target = $("#"+$(this).attr("data-toolbar")),cls = arguments[0];
		tog.click(function(event){
			tog.toggleClass(cls);
			target.toggleClass(cls);
			event.preventDefault();
		});
	},
	_toggle:function(){  /* 单方面触发的显隐样式    */
		var tog = $(this),target = $("#"+$(this).attr("data-develop")),cls = arguments[0];
		tog.click(function(evnet){
			target.toggleClass(cls);
			event.preventDefault();
		});
	},
	_dropDown:function(){
		var self = $(this),
			dir = self.attr("data-toggle"),
			tri = '<b class="caret arrow-'+dir+'" />',
			menus = self.find("+.submenu");
		
		$(self).append($(tri)).bind("click",function(event){
			if(menus.eq(self.index($(this))).css('display') != "none"){ menus.eq(self.index($(this))).slideUp(); return; }
			menus.slideUp().eq(self.index($(this))).slideDown();
			event.preventDefault();
		});
	},
	longPress : function(time,which,callBack){
       time = time || 1000;
       var timer = null;
       $(this).mousedown(function(event){
            var e = event;
            var _this = $(this);
            
            timer = setInterval(function(){
                if(e.which == which){
                	typeof callBack == 'function'?(function(){console.log(1);callBack(_this);})():void 0;
                }
            },time);
       }).mouseup(function(){clearTimeout(timer);});
    },
});

/* 在主导航内容超出容器时改变左右按钮的状态    */
function mainNavLayout(){
	if($(".main-nav .nav").width()<$(".main-nav .nav li").width()*1.05*$(".main-nav .nav li").length){
		$("#main-scroll-left,#main-scroll-right").addClass("active");
	}else{ $("#main-scroll-left,#main-scroll-right").removeClass("active"); }
}

/*exe*/
$(document).ready(function(){
	$("[toolbar-trigger]")._toolsbar("shrink");
	$("[toolbar-dev-trigger]")._toggle("show");
	$("#main-scroll-left").longPress(50,1,function(){
		$(".main-nav .nav").scrollLeft($(".main-nav .nav").scrollLeft()-10);
	});
	$("#main-scroll-right").longPress(50,1,function(){
		$(".main-nav .nav").scrollLeft($(".main-nav .nav").scrollLeft()+10);
	});
	$(".side-nav a[data-toggle]")._dropDown();
	mainNavLayout();
	window.onresize = function(){ mainNavLayout();};
	
	/*  use bootstrap.js  */
	$('.nav-tabs a').click(function (event) {
	  event.preventDefault();
	  $(this).tab('show');
	});
});
		
})(jQuery);
